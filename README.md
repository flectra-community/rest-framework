# Flectra Community / rest-framework

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[base_rest_demo](base_rest_demo/) | 2.0.3.0.1|         Demo addon for Base REST
[datamodel](datamodel/) | 2.0.3.0.0|         This addon allows you to define simple data models supporting        serialization/deserialization
[rest_log](rest_log/) | 2.0.1.0.2| Track REST API calls into DB
[base_rest](base_rest/) | 2.0.3.0.5|         Develop your own high level REST APIs for Odoo thanks to this addon.        
[graphql_base](graphql_base/) | 2.0.1.0.0|         Base GraphQL/GraphiQL controller
[base_rest_datamodel](base_rest_datamodel/) | 2.0.3.0.0|         Datamodel binding for base_rest
[graphql_demo](graphql_demo/) | 2.0.1.0.0| GraphQL Demo


