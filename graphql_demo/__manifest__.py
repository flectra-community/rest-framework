# Copyright 2018 ACSONE SA/NV
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).

{
    "name": "GraphQL Demo",
    "version": "2.0.1.0.0",
    "license": "LGPL-3",
    "author": "ACSONE SA/NV, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/rest-framework",
    "depends": ["graphql_base"],
    "external_dependencies": {"python": ["graphene"]},
    "development_status": "Beta",
    "maintainers": ["sbidoul"],
    "installable": False,
}
